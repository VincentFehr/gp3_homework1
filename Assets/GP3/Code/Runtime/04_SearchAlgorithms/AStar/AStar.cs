﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GP3._04_SearchAlgorithms.AStar
{
	public class AStar : SearchBase
	{
		protected override void InitializeSearch()
		{
			_startNode = _startMarker.ClosestGridNode;
			_startNode.Heuristic = GetHeuristic(_startNode, _startNode);
			_startNode.CostSoFar = 0;

			foreach (GridNode gridNode in _visited.Keys)
			{
				gridNode.Reset();
			}
			_goalNode = _endMarker.ClosestGridNode;
			
			_visited = new Dictionary<GridNode, GridNode>();
			
			_openList = new List<GridNode>();
			
			_openList.Add(_startNode);

		}

		protected override bool StepToGoal()
		{

			// Order the _openList by their Heuristic + their CostSoFar
			_openList = _openList.OrderBy(n => n.Heuristic + n.CostSoFar).ToList();
			
			
			GridNode current = _openList[0];

			// goal found
			if (current == _goalNode)
			{
				return true;
			}

			
			foreach (GridNode next in current.Neighbours)
			{
				if (next.IsWall)
				{
					continue;
				}

				// The calculation to check if the heuristic is still admissable
				float pathCost = current.CostSoFar + GetHeuristic(current,next);
				
				bool alreadyVisited = _visited.ContainsKey(next);

				// Has this Node already been visited?
				if (alreadyVisited)
				{
					// If yes then see if the paths cost is lower than the cost so far
					if (pathCost < next.CostSoFar)
					{
						// If yes then set the next node to be the current, add next to the _openlist and change the Heuristic and the CostSoFar of the former next
						next.CostSoFar = pathCost;
						next.Heuristic = GetHeuristic(_goalNode, next);
						_visited[next] = current;
						_openList.Add(next);
						next.SetGridNodeSearchState(GridNodeSearchState.Queue);
					}
				}
				else
				{
					// If no then add it to the lists and set its CostSoFar and Heuristic
					_openList.Add(next);
					_visited.Add(next, current);

					next.CostSoFar = pathCost;
					next.Heuristic = GetHeuristic(_goalNode, next);
					next.SetGridNodeSearchState(GridNodeSearchState.Queue);
				}
			}
			// If we are done adding the current node to the _visited list then remove it from the _openlist
			_openList.Remove(current);
			current.SetGridNodeSearchState(GridNodeSearchState.Processed);
			// not yet finished
			return false;
		}

		private float GetHeuristic(GridNode goal, GridNode next)
		{
			return (goal.transform.position - next.transform.position).magnitude;
		}
	}
}
