﻿using System;
using System.Collections.Generic;
using System.Globalization;
using GP3._04_SearchAlgorithms.BFS;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace GP3._04_SearchAlgorithms
{
	public class GridNode : MonoBehaviour
	{
		[SerializeField] private SpriteRenderer _image = default;
		[SerializeField] private SpriteRenderer _searchStateImage = default;
		private TextMeshPro _tmp;
		
		
		// Serialized values that are tweakable in the inspector
		[Header("Costs")]
		[SerializeField][Tooltip("The cost of walls")]
		private float _costWall = float.MaxValue;
		public float CostWall => _costWall;
		
		[SerializeField][Tooltip("The cost of ground")]
		private float _costGround = 1;
		public float CostGround => _costGround;

		[SerializeField][Tooltip("The cost of water")]
		private float _costWater = 10;
		public float CostWater => _costWater;

		[SerializeField][Tooltip("The cost of bridges")]
		private float _costBridge = 5;
		public float CostBridge => _costBridge;
		public float Cost
		{
			get
			{
				switch (_type)
				{
					case GridNodeType.Ground:
						return CostGround;
					case GridNodeType.Wall:
						return CostWall;
					case GridNodeType.Water:
						return CostWater;
					// Added Bridge Type
					case GridNodeType.Bridge:
						return CostBridge;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public float CostSoFar { get; set; }
		public IEnumerable<GridNode> Neighbours => _neighbours;
		public bool IsWall => _type == GridNodeType.Wall;
		public float Heuristic { get; set; }
		private List<GridNode> _neighbours = new List<GridNode>();
		
		[ShowInInspector]
		private GridNodeType _type;
		private GridNodeSearchState _searchState;

		public void Init()
		{
			Reset();
			FindNeighbours();
			SetGridNodeType(GridNodeType.Ground);
			SetGridNodeSearchState(GridNodeSearchState.None);
			_searchStateImage.enabled = false;
			
			// Get TMP from the child
			_tmp = GetComponentInChildren<TextMeshPro>();
			
			// Let TMP display each gridnodes cost whenever you start it
			_tmp.text = "" + _costGround;
		}

		private void OnMouseDown()
		{
			// Add a TMP + their cost to their switch statement so that the number that is displayed will change with their type
			switch (_type)
			{
				case GridNodeType.Ground:
					SetGridNodeType(GridNodeType.Wall);
					_tmp.text = "" + _costWall;
					break;
				case GridNodeType.Wall:
					SetGridNodeType(GridNodeType.Water);
					_tmp.text = "" + _costWater;
					break;
				case GridNodeType.Water:
					SetGridNodeType(GridNodeType.Bridge);
					_tmp.text = "" + _costBridge;
					break;
				case GridNodeType.Bridge:
					SetGridNodeType(GridNodeType.Ground);
					_tmp.text = "" + _costGround;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public void Reset()
		{
			SetGridNodeSearchState(GridNodeSearchState.None);
			CostSoFar = 0;
			
			// Resets only the visual cost of each node
			switch (_type)
			{
				case GridNodeType.Ground:
					_tmp.text = "" + _costGround;
					break;
				case GridNodeType.Wall:
					_tmp.text = "" + _costWall;
					break;
				case GridNodeType.Water:
					_tmp.text = "" + _costWater;
					break;
				case GridNodeType.Bridge:
					_tmp.text = "" + _costBridge;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public void SetGridNodeSearchState(GridNodeSearchState state)
		{
			_tmp = GetComponentInChildren<TextMeshPro>();
			_searchState = state;
			_searchStateImage.enabled = true;
			switch (state)
			{
				case GridNodeSearchState.None:
					_searchStateImage.color = BreadthFirstSearchSettings.Instance.GroundNodeColor;
					_tmp.text = (Heuristic + CostSoFar).ToString("0.0", CultureInfo.InvariantCulture);
					break;
				case GridNodeSearchState.Queue:
					_searchStateImage.color = BreadthFirstSearchSettings.Instance.QueueNodeColor;
					_tmp.text = (Heuristic + CostSoFar).ToString("0.0", CultureInfo.InvariantCulture);
					break;
				case GridNodeSearchState.Processed:
					_searchStateImage.color = BreadthFirstSearchSettings.Instance.ProcessedNodeColor;
					_tmp.text = (Heuristic + CostSoFar).ToString("0.0", CultureInfo.InvariantCulture);
					break;
				case GridNodeSearchState.PartOfPath:
					_searchStateImage.color = BreadthFirstSearchSettings.Instance.PathNodeColor;
					_tmp.text = (Heuristic + CostSoFar).ToString("0.0", CultureInfo.InvariantCulture);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(state), state, null);
			}
		}

		private void SetGridNodeType(GridNodeType type)
		{
			_type = type;
			switch (type)
			{
				case GridNodeType.Ground:
					_image.color = BreadthFirstSearchSettings.Instance.GroundNodeColor;
					break;
				case GridNodeType.Wall:
					_image.color = BreadthFirstSearchSettings.Instance.WallNodeColor;
					break;
				// Added Bridge Node Color
				case GridNodeType.Bridge:
					_image.color = BreadthFirstSearchSettings.Instance.BridgeNodeColor;
					break;
				case GridNodeType.Water:
					_image.color = BreadthFirstSearchSettings.Instance.WaterNodeColor;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}
		}

		private void FindNeighbours()
		{
			RaycastHit2D raycastHit2D = Physics2D.Linecast(transform.position, transform.position + Vector3.up, ~LayerMask.NameToLayer("Grid"));
			if ((raycastHit2D.collider != null) && raycastHit2D.collider.TryGetComponent(out GridNode node))
			{
				_neighbours.Add(node);
			}
			raycastHit2D = Physics2D.Linecast(transform.position, transform.position + Vector3.right, ~LayerMask.NameToLayer("Grid"));
			if ((raycastHit2D.collider != null) && raycastHit2D.collider.TryGetComponent(out node))
			{
				_neighbours.Add(node);
			}
			raycastHit2D = Physics2D.Linecast(transform.position, transform.position + Vector3.down, ~LayerMask.NameToLayer("Grid"));
			if ((raycastHit2D.collider != null) && raycastHit2D.collider.TryGetComponent(out node))
			{
				_neighbours.Add(node);
			}
			raycastHit2D = Physics2D.Linecast(transform.position, transform.position + Vector3.left, ~LayerMask.NameToLayer("Grid"));
			if ((raycastHit2D.collider != null) && raycastHit2D.collider.TryGetComponent(out node))
			{
				_neighbours.Add(node);
			}
		}
	}
}