﻿using System.Collections.Generic;

namespace GP3._04_SearchAlgorithms.DFS
{
    public class DepthFirstSearch : SearchBase
    {
        private Stack<GridNode> _stackList;

        protected override void InitializeSearch()
        {
            _startNode = _startMarker.ClosestGridNode;
            _goalNode = _endMarker.ClosestGridNode;

            foreach (GridNode gridNode in _visited.Keys)
            {
                gridNode.Reset();
            }
            
            _visited = new Dictionary<GridNode, GridNode>();
            _stackList = new Stack<GridNode>();
            _stackList.Push(_startNode);
        }
        // Not working (╯°□°)╯︵ ┻━┻
        protected override bool StepToGoal()
        {
            GridNode current = _stackList.Peek();
			
            // goal found
            if (current == _goalNode)
            {
                return true;
            }

            foreach (GridNode next in current.Neighbours)
            {
                if (next.IsWall)
                {
                    continue;
                }
                
                bool alreadyVisited = _visited.ContainsKey(next);

                if (alreadyVisited)
                {
                    _stackList.Push(next);
                    _visited.Add(next, current);
                    next.SetGridNodeSearchState(GridNodeSearchState.Queue);
                }
                else
                {
                    
                }
            }

            current.SetGridNodeSearchState(GridNodeSearchState.Processed);
            // not yet finished
            return false;
        }
    }
}

