﻿using TMPro;
using UnityEngine;

namespace GP3._05_FSM.Quests
{
	public class QuestNPC : MonoBehaviour
	{
		[SerializeField] [Tooltip("NPC Dialogue Text")]
		private TextMeshProUGUI _npcBark = default;
		[SerializeField] [Tooltip("Item to Collect")]
		private QuestItem _questItem;
		[SerializeField] [Tooltip("Quests Icon shown available state")]
		private GameObject _availableStateMesh;
		[SerializeField] [Tooltip("Quests Icon shown quest task done state")]
		private GameObject _questTaskDoneStateMesh;

		// Time before the quest runs out
		[SerializeField] [Tooltip("Time before the quest runs out")]
		private float _timeToCollect;

		// This value resets the _timeToCollect value to its assigned float
		private float _timeReset;
		// Checks if you collected the item in time, if not then its true
		private bool _notCollectedInTime;
		// Checks if the countdown is already running
		private bool _timeIsGoingDown;


		public static QuestAvailableState QuestAvailableState = new QuestAvailableState();
		public static QuestActiveState QuestActiveState = new QuestActiveState();
		public static QuestTaskDoneState QuestTaskDoneState = new QuestTaskDoneState();
		public static QuestDoneState QuestDoneState = new QuestDoneState();
		public static QuestFailedState QuestFailedState = new QuestFailedState();
		
		private IQuestState _currentState;
		private GameObject _currentQuestMarker;

		public bool IsPlayerClose { get; private set; }
		public bool RequirementsMet => _questItem.IsCollected;
		public bool RequirementsNotMet => _notCollectedInTime;
		public GameObject AvailableStateMesh => _availableStateMesh;
		public GameObject QuestTaskDoneStateMesh => _questTaskDoneStateMesh;
		public QuestItem QuestItem => _questItem;

		private void Awake()
		{
			_currentState = QuestAvailableState;
			// Set _timeReset equal to _timeToCollect
			_timeReset = _timeToCollect;
		}

		private void Update()
		{
			IQuestState nextState = _currentState.Execute(this);
			if (nextState != _currentState)
			{
				_currentState.Exit(this);
				_currentState = nextState;
				_currentState.Enter(this);
			}

			// Reassign the _timeReset value so its always the serialized value from the inspector but only if the time is not going down and there was a change in the inspector
			if (_timeReset != _timeToCollect && !_timeIsGoingDown)
			{
				_timeReset = _timeToCollect;
			}
		}

		// Starts counting down by the value of _timeToCollect
		public void QuestTimer()
		{
			// Stops the countdown
			_timeIsGoingDown = true;
			// The countdown thats ticking down
			_timeToCollect -= Time.deltaTime;
			if (_timeToCollect <= 0)
			{
				// If the time runs out before you reach the item, then _notCollectedInTime will be set to true while _timeToCollect is reset to its assigned value in the inspector
				_notCollectedInTime = true;
				_timeToCollect = _timeReset;
				_timeIsGoingDown = false;
			}
			else
			{
				// If you reached the item in time then set _notCollectedInTime to false
				_notCollectedInTime = false;
			}
		}
		private void OnTriggerEnter(Collider other)
		{
			if (other.CompareTag("Player"))
			{
				IsPlayerClose = true;
				ShowMessage();
			}
		}

		private void OnTriggerExit(Collider other)
		{
			if (other.CompareTag("Player"))
			{
				IsPlayerClose = false;
				HideMessage();
			}
		}

		private void HideMessage()
		{
			_npcBark.alpha = 0;
		}

		private void ShowMessage()
		{
			_npcBark.alpha = 1;
		}

		public void DisplayQuestIcon(GameObject availableStateMesh)
		{
			Destroy(_currentQuestMarker);

			if (availableStateMesh != null)
			{
				_currentQuestMarker = Instantiate(availableStateMesh, transform);
			}
		}

		public void SetNPCAnswer(string answer)
		{
			_npcBark.text = answer;
		}
	}
}