﻿using UnityEngine;

namespace GP3._05_FSM.Quests
{
	public interface IQuestState
	{
		IQuestState Execute(QuestNPC npc);
		void Enter(QuestNPC npc);
		void Exit(QuestNPC npc);
	}
	
	public class QuestAvailableState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			if (Input.GetMouseButtonDown(0) && npc.IsPlayerClose)
			{
				return QuestNPC.QuestActiveState;
			}
			
			return QuestNPC.QuestAvailableState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.DisplayQuestIcon(npc.AvailableStateMesh);
			npc.SetNPCAnswer("Hey you! Go get that item for me, would ya?");
		}

		public void Exit(QuestNPC npc)
		{
			npc.DisplayQuestIcon(null);
			npc.SetNPCAnswer("");
		}
	}
	
	public class QuestActiveState : IQuestState
     	{
     		public IQuestState Execute(QuestNPC npc)
     		{
	            // Activates the quest timer
	            npc.QuestTimer();

	            if (npc.RequirementsMet)
     			{
     				return QuestNPC.QuestTaskDoneState;
     			}
                if (npc.RequirementsNotMet)
                {
	                // If the item is not collected in time then return questfailedstate
	                return QuestNPC.QuestFailedState;
                }
     			
     			return QuestNPC.QuestActiveState;
     		}
     
     		public void Enter(QuestNPC npc)
            {
	            npc.QuestItem.Activate();
     			npc.SetNPCAnswer("Remember to bring me my item please");
     		}
     
     		public void Exit(QuestNPC npc)
     		{
     			npc.SetNPCAnswer("");
     		}
     	}
	
	// My implemented state that sets the quest state to being failed
	public class QuestFailedState : IQuestState
	{
		// Time before the npc resets to its questavailablestate
		private float timeToReset = 3;
		public IQuestState Execute(QuestNPC npc)
		{
			// Counts down the reset timer
			timeToReset -= Time.deltaTime;

			if (timeToReset <= 0)
			{
				// If timeToReset ran out then go back to questavailablestate
				return QuestNPC.QuestAvailableState;
				
			}
			
			return QuestNPC.QuestFailedState;
		}

		public void Enter(QuestNPC npc)
		{
			// Sets the active state of the item to false
			npc.QuestItem.NotCollected();
			// NPC text if you dont reach the item in time
			npc.SetNPCAnswer("Oh shucks...you were too slow!");
		}

		public void Exit(QuestNPC npc)
		{
			// Reset the timeToReset value and TMP as you leave the state
			timeToReset = 3;
			npc.SetNPCAnswer("");
		}
	}
	public class QuestTaskDoneState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			if (npc.IsPlayerClose && Input.GetMouseButtonDown(0))
			{
				return QuestNPC.QuestDoneState;
			}

			return QuestNPC.QuestTaskDoneState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.DisplayQuestIcon(npc.QuestTaskDoneStateMesh);
			npc.SetNPCAnswer("Ay what a great finding! Myyyy precious");
		}

		public void Exit(QuestNPC npc)
		{
			npc.DisplayQuestIcon(null);
			npc.SetNPCAnswer("");
		}
	}
	
	public class QuestDoneState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			return QuestNPC.QuestDoneState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.SetNPCAnswer("My precious..");
		}

		public void Exit(QuestNPC npc)
		{
		}
	}
}